DROP TABLE IF EXISTS cat;
CREATE TABLE cat
(
 id integer NOT NULL ,
 name varchar(50) DEFAULT NULL,
 url varchar(200) DEFAULT NULL,
 PRIMARY KEY (id)
);