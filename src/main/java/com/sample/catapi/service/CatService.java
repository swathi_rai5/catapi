package com.sample.catapi.service;

import java.util.List;

import com.sample.catapi.entity.Cat;

public interface CatService {
	List<Cat> findAll();

	void insertCat(Cat cat);

	void updateCat(Cat cat);

	void executeUpdateCat(Cat cat);

	void deleteCat(Cat cat);
	
}
