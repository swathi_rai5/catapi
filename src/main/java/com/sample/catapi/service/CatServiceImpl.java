package com.sample.catapi.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.sample.catapi.dao.CatDao;
import com.sample.catapi.entity.Cat;
@Component
public class CatServiceImpl implements CatService{
	@Resource 
	CatDao catDao;
	@Override
	public List<Cat> findAll() {
		return catDao.findAll();
	}
	@Override
	public void insertCat(Cat cat) {
		catDao.insertCat(cat);
		
	}
	@Override
	public void updateCat(Cat cat) {
		catDao.updateCat(cat);
		
	}
	@Override
	public void executeUpdateCat(Cat cat) {
		catDao.executeUpdateCat(cat);
		
	}

	@Override
	public void deleteCat(Cat cat) {
		catDao.deleteCat(cat);
		
	}
}
