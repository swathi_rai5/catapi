package com.sample.catapi.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Cat {

	int id;
	String name;
	String url;
	
	@JsonProperty("id")
	public int getCatId() {
		return id;
	}
	public void setCatId(int id) {
		this.id = id;
	}
	
	@JsonProperty("name")
	public String getCatName() {
		return name;
	}
	public void setCatName(String name) {
		this.name = name;
	}
	
	@JsonProperty("url")
	public String getCatUrl() {
		return url;
	}
	public void setCatUrl(String url) {
		this.url = url;
	}
	
	
}
