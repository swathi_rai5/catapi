package com.sample.catapi.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sample.catapi.entity.Cat;
import com.sample.catapi.mapper.CatRowMapper;
@Repository
public class CatDaoImpl implements CatDao{
	
	public CatDaoImpl(NamedParameterJdbcTemplate template) {  
        this.template = template;  
}  
	NamedParameterJdbcTemplate template;  

	@Override
	public List<Cat> findAll() {
		return template.query("select * from cat", new CatRowMapper());
	}
	@Override
	public void insertCat(Cat cat) {
		 final String sql = "insert into cat(id, name, url) values(:id, :name, :url)";
		 
	        KeyHolder holder = new GeneratedKeyHolder();
	        SqlParameterSource param = new MapSqlParameterSource()
					.addValue("id", cat.getCatId())
					.addValue("name", cat.getCatName())
					.addValue("url", cat.getCatUrl());
	        template.update(sql,param, holder);
	 
	}
	
	@Override
	public void updateCat(Cat cat) {
		 final String sql = "update cat set name=:name, url=:url where id=:id";
		 
	        KeyHolder holder = new GeneratedKeyHolder();
	        SqlParameterSource param = new MapSqlParameterSource()
					.addValue("id", cat.getCatId())
					.addValue("name", cat.getCatName())
					.addValue("url", cat.getCatUrl());
	        template.update(sql,param, holder);
	 
	}
	
	@Override
	public void executeUpdateCat(Cat cat) {
		 final String sql = "update cat set name=:name, url=:url where id=:id";
			 

		 Map<String,Object> map=new HashMap<String,Object>();  
		 map.put("id", cat.getCatId());
		 map.put("name", cat.getCatName());
		 map.put("url", cat.getCatUrl());
	
		 template.execute(sql,map,new PreparedStatementCallback<Object>() {  
			    @Override  
			    public Object doInPreparedStatement(PreparedStatement ps)  
			            throws SQLException, DataAccessException {  
			        return ps.executeUpdate();  
			    }  
			});  

	 
	}
	
	@Override
	public void deleteCat(Cat cat) {
		 final String sql = "delete from cat where id=:id";
			 

		 Map<String,Object> map=new HashMap<String,Object>();  
		 map.put("id", cat.getCatId());
	
		 template.execute(sql,map,new PreparedStatementCallback<Object>() {  
			    @Override  
			    public Object doInPreparedStatement(PreparedStatement ps)  
			            throws SQLException, DataAccessException {  
			        return ps.executeUpdate();  
			    }  
			});  

	 
	}
	
}
