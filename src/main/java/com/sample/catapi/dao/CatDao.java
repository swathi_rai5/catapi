package com.sample.catapi.dao;

import java.util.List;

import com.sample.catapi.entity.Cat;

public interface CatDao {

	List<Cat> findAll();

	void insertCat(Cat cat);

	void updateCat(Cat cat);

	void executeUpdateCat(Cat cat);

	public void deleteCat(Cat cat);
}
