package com.sample.catapi.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sample.catapi.entity.Cat;

public class CatRowMapper implements RowMapper<Cat> {

	@Override
	public Cat mapRow(ResultSet rs, int arg1) throws SQLException {
		Cat cat = new Cat();
		cat.setCatId(rs.getInt("id"));
		cat.setCatName(rs.getString("name"));
		cat.setCatUrl(rs.getString("url"));
 
        return cat;
	}


}
