package com.sample.catapi.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sample.catapi.entity.Cat;
import com.sample.catapi.service.CatService;

@RestController
@RequestMapping("/")
public class ApplicationController {

	@Resource 
	CatService catService;
	
	@GetMapping(value = "listCats")
	public List<Cat> getCat() {
		return catService.findAll();
	
	}
	
	@PostMapping(value = "createCat")
	public void createCat(@RequestBody Cat cat) {
		catService.insertCat(cat);
	
	}
	@PutMapping(value = "updateCat")
	public void updateCat(@RequestBody Cat cat) {
		catService.updateCat(cat);
	
	}
	@PutMapping(value = "executeUpdateCat")
	public void executeUpdateCat(@RequestBody Cat cat) {
		catService.executeUpdateCat(cat);
	
	}
	
	@DeleteMapping(value = "deleteCat")
	public void deleteCat(@RequestBody Cat cat) {
		catService.deleteCat(cat);
	
	}
	
	
}
