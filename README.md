# catapi

Clone the project with: **git clone https://swathi_rai5@bitbucket.org/swathi_rai5/catapi.git**

Command to run the project:

```
mvn spring-boot:run
```

## APIs
### Add a cat.

```
/createCat [POST]

Content-Type: application/json

{
    "id": 2,
    "name": "Siberian",
    "url": "https://placekitten.com/630/370"
}
```

When succeeded 200 Status code.

### Get list of cats.

```
/listCats [GET]
```

Gets the list of cats available in the database.

### Update the cat data.

```
/updateCat [PUT]

Content-Type: application/json

{
    "id": 2,
    "name": "Ragdoll",
    "url": "https://placekitten.com/630/370"
}
```

Updates the data of the cat provided in the request body.


### Delete the cat data with given id.

```
/deleteCat [DELETE]

Content-Type: application/json

{
    "id": 2
}
```

Deletes the data of the cat with the id given in the request body.
